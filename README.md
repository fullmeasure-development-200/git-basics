# Using git commands
## Starting a new thing
- Every time you start a new thing (e.g., You want to change something about all of the templates), do the following.
```git status```
- If everything is clean (i.e., no red or green files are shown), you are in a good place. Skip to step 3
- If things are not clean (i.e. there are red or green files) do the following:
1. Make sure you are on a branch that is not `master` ```git branch```
- The branch you are on will be highlighted in green.
2. If you are not on master run ```git add -A``` which will stage your files and ```git commit -m 'some message'``` which will commit your files.
3. ```git checkout master``` - master is the clean branch everyone is working off of. You shoud only ever pull when you are on a master branch
4. ```git pull origin master``` - this will pull any changes from github.com
5 ```git checkout -b sam/your_branch_name``` This will checkout a branch that is up to date with master so you can start making changes

## Finishing a thing
- At any point, but certainly after you finish making changes you will want to do the following:
1. ```git status``` - This will give you a list of files you have changed
a. If there are any files that you did not mean to change you can run ```git checkout path/to/file.txt``` This will revert that file to what was on master
2. ```git add -A``` and ```git commit -m 'some message'```
a. If you did not mean to make a commit you can always run ```undo_commit``` and ```undo_stage``` to get back to a good place
3. ```git push origin sam/your_branch_name``` - This will push your changes to a branch on github
4. Go to github.com > click on jameson > click on branches 
5. Find your branch and click Create Pull Request
6. At this point anyone can view your changes and make comments. 
7. When you are happy with the changes, Click on the pull request > Click the Conversation tab > Click merge pull request
8. Your changes are now on master

## Syncing your changes back to your local master branch
1. ```git checkout master```
2. ```git pull origin master```